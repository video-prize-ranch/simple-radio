# Simple Radio
A simple internet radio player for the web.

## Features
- [x] - PWA with native feel
- [x] - Play HLS audio
- [x] - Import/export station lists
- [x] - YouTube support

## Usage

### Development

Install dependencies
```
pnpm i
```

Start development server
```
pnpm run dev
```

Build
```
pnpm run build
```