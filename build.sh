#!/bin/sh
pnpm run build
mv dist /tmp/simple-radio-tmp
mv node_modules /tmp/simple-radio-nm
git branch -D pages
git switch --orphan pages
rm -rf *
mv /tmp/simple-radio-tmp/* .
rm -rf /tmp/simple-radio-tmp
git add .
git commit -m "Update site"
git push --force --set-upstream origin pages
git checkout main
mv /tmp/simple-radio-nm node_modules
