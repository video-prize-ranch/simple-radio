module.exports = {
  content: ["./src/**/*.{html,js,svelte,ts}"],
  plugins: [require("daisyui"), require('vidstack/tailwind.cjs')],
};